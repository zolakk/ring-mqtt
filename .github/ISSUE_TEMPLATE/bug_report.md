---
name: Bug report
about: Submit an issue with ring-mqtt
title: ''
labels: ''
assignees: tsightler
---

If you are using the Ring Devices Addon for Home Assistant, and the issue you are reporting is related to installing, configuring or operating the script underneath that platform, you should probably not be opening an issue on this page, but rather on the Ring Devices Addon page:
https://github.com/tsightler/ring-mqtt-ha-addon


### !!!!PLEASE READ THIS FIRST!!!!! ###
This is a community project.  I orginally created this project for my own personal use and decided to share it because I thought other users might find it useful, but I have no association with Ring, other than owning a few of their products, and I'm not your personal support person.  In general I will do my best to help, but I answer these issues on my own personal time and my effort to do so will largely be commensurate to the effort that you put it.  If you open an issue but don't have time to respond to questions, provide logs, etc, then I will close the issue without much thought.  I'm sorry for being so blunt, but the number of people that open issues with a single sentence like "I can't get snapshots to work", or that open an issue and then never respond to any requests for information, has grown to the point that it really makes working on this project less than enjoyable.

### Important Note ###
If you are using the Ring Devices Addon for Home Assistant, and the issue you are reporting is related to installing, configuring or operating the script underneath that platform, you should probably not be opening an issue on this page, but rather on the Ring Devices Addon page:
https://github.com/tsightler/ring-mqtt-ha-addon

**Describe the bug**  
A clear and concise description of the issue.  Please enter a brief summary of the issue in the title above as well.

**Describe your environment**  
Please include details on your enviornment, including OS versions, platform, etc.

**Describe your settings and what you've tried**  
Please make sure to share you configuration settings (other than sensitive information like token, etc) and anything you have done to attempt to solve the problem to this point.

**Debug Logs**  
In most cases the only way to effectively troubleshoot an issue is to review the logs, without this, I'll mostly just be guessing at the problem.  Please run the script with DEBUG=ring-* enabled (note this is enabled by default for the addon and Docker versions) and collect logs.  Logs can contain potentially sensitive information so you probably do not want to post them publicly on Github, which I certainly understand, so feel free to send the logs, or a link to download them, to my personal email address, which is the same username and gmail.  Please include a link to the open issue.
